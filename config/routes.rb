Rails.application.routes.draw do

  get '/services/data/v1/contracts/:pramata_number/:entity_api_name/:entity_id/:field_name/:id/citation/preview/' => 'citation#preview'
  get '/services/data/v1/contracts/:pramata_number/:entity_api_name/:entity_id/:field_name/:id/citation/pdf/' => 'citation#pdf'    

  namespace :services do 
    namespace :data do 
      namespace :v1 do 
       
        get 'contracts/:pramata_number' => 'contracts#show'
        get 'contracts/:pramata_number/pdf' => 'contracts#pdf'
        get 'contracts/:pramata_number/categories' => 'contracts#categories'
        get 'contracts/:pramata_number/pdf_file' => 'contracts#pdf_file'
        get 'contracts/:pramata_number/:entity_api_name' => 'contracts#show'
        get 'customers' => 'customers#index'
        get 'customers/:id' => 'customers#show'
        get 'customers/:customer_id/contracts' => 'contracts#index'
      end
    end
  end
end