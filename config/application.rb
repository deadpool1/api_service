require_relative 'boot'

require 'rails/all'
require_relative "../lib/pramata"
# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ApiService
  class Application < Rails::Application
    config.middleware.use Pramata::Middleware::Elevator  	
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1
    # config.middleware.insert_before 0, "Rack::Cors" do
    #   allow do
    #     origins '*'
    #     resource '*', :headers => :any, :methods => [:get, :post, :options]
    #   end
    # end
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.active_support.escape_html_entities_in_json = false     
  end
end
