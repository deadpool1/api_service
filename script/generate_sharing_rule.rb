Apartment::Tenant.switch!('alpha')
UserRole.all.each do |role|
  Contract.find_in_batches(batch_size: 1000) do |cs|
    cs.each do |c|
    SharingRule.create(
      object_type: 'Contract', 
      object_id: c.id, 
      role_id: role.id
    )  
    p "created sharing rule for role_id: #{role.id} and contract_id: #{c.id}"
end
  end
end
