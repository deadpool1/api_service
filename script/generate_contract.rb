Apartment::Tenant.switch!('alpha')
MILLION = 1_000_000
MILLION.times do |time|
	c = Contract.new({
		"display_name"=>"Alpha Master Service Agreement No: #{time}", 
		"description"=>"", 
		"contract_type_id"=>2, 
		"parent_id"=>0, 
		"effective_date"=>"29-08-2016", 
		"contract_model_id"=>7, 
		"company_group_id"=>13, 
		"pontus_number"=>10000+time, 
		"contract_po_status_type_id"=>15, 
		"intake_contract_type_id"=>0, 
		"is_nonstandard"=>false, 
		"contract_request_id"=>nil, 
		"no_parent_found"=>true, 
		"total_pages"=>1, 
		"milestone_id"=>6, 
		"file_upload_id"=>nil, 
		"is_current"=>true, 
		"is_executed"=>true, 
		"is_legacy"=>false, 
		"is_complex"=>false, 
		"is_ocred"=>false, 
		"sla_end_time"=>nil, 
		"sla_met"=>nil, 
		"sla_duration"=>5, 
		"delayed_by_exception"=>false, 
		"group_data_id"=>nil, 
		"label"=>nil, 
		"fts_pramata_number"=>10000+time, 
		"availability_in_keyword_search"=>"Not Available", 
		"is_alert_enabled"=>true, 
		"primary_customer_id"=>nil, 
		"is_dirty"=>false, 
		"status"=>nil
	})
	c.save!
	p "Created Contract: #{c.id}"
end