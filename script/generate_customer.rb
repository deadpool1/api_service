Apartment::Tenant.switch!('alpha')
TEN_MILLION = 10_000_000
TEN_MILLION.times do |time|
	c = Customer.new({
		"primary_name"=>(0...8).map { (65 + rand(26)).chr }.join,
		"customer_number"=>"SM"+time.to_s,
		"source_system"=>"Salesforce", 
		"enterprise_name"=>"Sun Java", 
		"trade_name"=>nil, 
		"legal_name"=>nil, 
		"parent_customer_number"=>nil, 
		"address"=>nil, 
		"current_revenue"=> time, 
		"alias_name_1"=>nil, 
		"alias_name_2"=>nil, 
		"alias_name_3"=>nil, 
		"duns_number"=>nil, 
		"buying_group"=>nil, 
		"website"=>nil, 
		"status"=>nil, 
		"custom_field_1"=>nil, 
		"custom_field_2"=>nil, 
		"custom_field_3"=>nil, 
		"custom_field_4"=>nil, 
		"custom_field_5"=>nil, 
		"custom_field_6"=>nil, 
		"custom_field_7"=>nil, 
		"custom_field_8"=>nil, 
		"custom_field_9"=>nil, 
		"custom_field_10"=>nil, 
		"created_at"=>nil, 
		"updated_at"=>nil, 
		"salesforce_account_id"=>nil, 
		"custom_field_11"=>nil
	})
	c.save!
	p "Created Customer: #{c.id}"
end