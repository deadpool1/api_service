require 'rails_helper'

SAMPLE_DATA_FOLDER = "#{Rails.root}/spec/data/citations/"
BASE_DOC_DIR = "#{Rails.root}/doc/raml/samples"

describe Services::Data::V1::EntitiesController do

  let(:token) { double :acceptable? => true }

  before do
    ApplicationController.any_instance.stub(:doorkeeper_token) { token }
  end  

  context " Entities API /services/data/v1/entities/1" do 
    it "should return 500 for not implemented" do
      get :show, {:id => "1"}
      response.code.should == "502"
      actual_response = JSON.parse(response.body)
    end 
  end
end