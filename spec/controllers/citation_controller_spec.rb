require 'rails_helper'

SAMPLE_DATA_FOLDER = "#{Rails.root}/spec/data/citations/"
BASE_DOC_DIR = "#{Rails.root}/doc/raml/samples"

RSpec.describe "Enhanced Citation Spec", :type => :request do
  let(:token) { double :acceptable? => true }

  before do
    ApplicationController.any_instance.stub(:doorkeeper_token) { token }
  end  

  context "Enhanced Citation" do 
    it "should generate the PDF with highlight of the citation coordinates" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_de.id}/citation/pdf/"

      response.status.should == 200      
      response.content_type.to_s.should == 'application/json'

      json_result = JSON.parse(response.body)
      actual_pdf_digest = Digest::MD5.hexdigest(Base64.decode64(json_result["data"]["attributes"]["file_bytes"]))
      
      actual_pdf_digest.should == @highlight_pdf_digest
      json_result["meta"]["content_type"].should == 'application/pdf'
    end

    it "should generate the PDF with highlight when rotated by 90 degrees" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_rotated_90_de.id}/citation/pdf/"

      response.status.should == 200      
      response.content_type.to_s.should == 'application/json'

      json_result = JSON.parse(response.body)
      degree_90_file = "#{Rails.root}/tmp/90_degree_test.pdf" 
      File.open(degree_90_file,"wb") do |f| 
        f.write(Base64.decode64(json_result["data"]["attributes"]["file_bytes"]))
      end
      generate_image_chksum(degree_90_file,1).should == @rotated_90_pdf_digest
      json_result["meta"]["content_type"].should == 'application/pdf'
    end

    it "should generate the PDF with highlight when rotated by 270 degrees" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_rotated_270_de.id}/citation/pdf/"

      response.status.should == 200      
      response.content_type.to_s.should == 'application/json'

      json_result = JSON.parse(response.body)
      actual_pdf_digest = Digest::MD5.hexdigest(
        Base64.decode64(json_result["data"]["attributes"]["file_bytes"])
      )
      
      degree_270_file = "#{Rails.root}/tmp/270_degree_test.pdf" 
      File.open(degree_270_file,"wb") do |f| 
        f.write(Base64.decode64(json_result["data"]["attributes"]["file_bytes"]))
      end
      generate_image_chksum(degree_270_file,2).should == @rotated_270_pdf_digest
      json_result["meta"]["content_type"].should == 'application/pdf'
    end

    it "should fallback to origami and generate the PDF with highlight when rotated by 270 degrees" do
      stubbed_pdfinfo_response = "Page 1 size:      612 x 792 pts (letter)"
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      Pramata::Citation::PdfHighlighter.any_instance.stub(:metadata_from_pdfinfo).and_return(stubbed_pdfinfo_response)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_rotated_270_de.id}/citation/pdf/"

      response.status.should == 200      
      response.content_type.to_s.should == 'application/json'

      json_result = JSON.parse(response.body)
      actual_pdf_digest = Digest::MD5.hexdigest(
        Base64.decode64(json_result["data"]["attributes"]["file_bytes"])
      )
      
      degree_270_file = "#{Rails.root}/tmp/270_degree_test.pdf" 
      File.open(degree_270_file,"wb") do |f| 
        f.write(Base64.decode64(json_result["data"]["attributes"]["file_bytes"]))
      end
      generate_image_chksum(degree_270_file,2).should == @rotated_270_pdf_digest
      json_result["meta"]["content_type"].should == 'application/pdf'
    end

    it "should fallback to 0 rotation if origami fails to generate the PDF with highlight when rotated by 270 degrees" do
      stubbed_pdfinfo_response = "Page 1 size:      612 x 792 pts (letter)"
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      Pramata::Citation::PdfHighlighter.any_instance.stub(:metadata_from_pdfinfo).and_return(stubbed_pdfinfo_response)
      Origami::PDF.stub(:read).and_raise("Origami Failure")
      
      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_de.id}/citation/pdf/"

      response.status.should == 200      
      response.content_type.to_s.should == 'application/json'

      json_result = JSON.parse(response.body)
      actual_pdf_digest = Digest::MD5.hexdigest(Base64.decode64(json_result["data"]["attributes"]["file_bytes"]))
      
      actual_pdf_digest.should == @highlight_pdf_digest
      json_result["meta"]["content_type"].should == 'application/pdf'
    end

    it "should return 404 if the provided data element Id is invalid for PDF Highlight" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/-1/citation/pdf/"
      response.status.should == 404

      # To Confirm that documentation is uptodate with implementation
      Pramata::Api::ResponseValidator.assert_doc_uptodate(
        "#{BASE_DOC_DIR}/api/v1/contracts/term/term_id/data_element/data_element_id/citation/pdf/404.json",
        JSON.parse(response.body))
    end

    it "should return 500 if the provided data element has no Page Number for PDF Highlight" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_invalid_page_de.id}/citation/pdf/"
      response.status.should == 500

      # To Confirm that documentation is uptodate with implementation
      Pramata::Api::ResponseValidator.assert_doc_uptodate(
        "#{BASE_DOC_DIR}/api/v1/contracts/term/term_id/data_element/data_element_id/citation/pdf/500.json",
        JSON.parse(response.body))        
    end

    it "should generate the Image Preview with highlight of the citation coordinates" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)

      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_de.id}/citation/preview/"

      response.status.should == 200      
      response.content_type.to_s.should == 'application/json'

      json_result = JSON.parse(response.body)
      actual_image_digest = Digest::MD5.hexdigest(Base64.decode64(json_result["data"]["attributes"]["file_bytes"]))
      
      actual_image_digest.should == @highlight_image_digest
      json_result["meta"]["content_type"].should == 'image/jpeg'     
    end


    it "should return 404 if the provided data element Id is invalid for Preview" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/-1/citation/preview/"
      response.status.should == 404

      # To Confirm that documentation is uptodate with implementation
      Pramata::Api::ResponseValidator.assert_doc_uptodate(
        "#{BASE_DOC_DIR}/api/v1/contracts/term/term_id/data_element/data_element_id/citation/preview/404.json",
        JSON.parse(response.body))     
    end

    it "should return 500 if the provided data element has no Page Number for Preview Highlight" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_invalid_page_de.id}/citation/preview/"
      response.status.should == 500

      # To Confirm that documentation is uptodate with implementation
      Pramata::Api::ResponseValidator.assert_doc_uptodate(
        "#{BASE_DOC_DIR}/api/v1/contracts/term/term_id/data_element/data_element_id/citation/preview/500.json",
      JSON.parse(response.body))
    end

    it "should have valid Data Folder" do
      original_data_folder = "#{Rails.root.to_s}/data/contract_file/po_number/"
      Pramata::Citation::PdfHighlighter.new(@enhanced_citation_de).send(:data_folder).should == original_data_folder
    end

    it "should set the performance audit for the Image Preview with highlight call" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)

      get "/api/v1/contract/10001/Termination_Notice/123/Name/#{@enhanced_citation_de.id}/citation/preview/"

      last_audit = ApiPerformanceAudit.last 
      last_audit.contract_term_data_element_id.should == @enhanced_citation_de.id  
      last_audit.annotation_id.should == Annotation.where(contract_term_data_element_id: @enhanced_citation_de.id).first.id
      last_audit.request_type.should == 'citation_preview'
      last_audit.total_time.should_not == nil  
      last_audit.pramata_number.should == @enhanced_citation_de.contract.pontus_number
    end    
  end 

  context "Classic Citation" do 
    it "should return the original contract PDF for classic citation" do
      Pramata::Citation::Highlighter.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get "/api/v1/contract/10006/Termination_Notice/123/Name/#{@classic_citation_de.id}/citation/pdf/"
      actual_pdf_digest = Digest::MD5.hexdigest(response.body)
      response.content_type.to_s.should == 'application/json'
  
      json_result = JSON.parse(response.body)
      actual_pdf_digest = Digest::MD5.hexdigest(Base64.decode64(json_result["data"]["attributes"]["file_bytes"]))
      actual_pdf_digest.should == @original_pdf_digest
    end
  end
  
  before(:all) do
    generate_pdf_digests
    cleanup_all_tables

    @cmtt = ContractModelTermType.find_or_create_by(name: "Test Term")
    @cmde = ContractModelDataElement.find_or_create_by(
      name: "Test Data Element",
      contract_model_term_type_id: @cmtt.id
    )

    @contract1 = Contract.find_or_create_by(pontus_number: 10001)
    @enhanced_citation_de = create_ctde(@cmtt,@cmde,@contract1)
    create_valid_annotation(@enhanced_citation_de,1)

    @contract2 = Contract.find_or_create_by(pontus_number: 10002)
    @enhanced_citation_rotated_90_de = create_ctde(@cmtt,@cmde,@contract2)
    create_valid_annotation(@enhanced_citation_rotated_90_de,1)

    @enhanced_citation_rotated_270_de = create_ctde(@cmtt,@cmde,@contract2)
    create_valid_annotation(@enhanced_citation_rotated_270_de,2)

    @contract3 = Contract.find_or_create_by(pontus_number: 10003)    
    @enhanced_citation_invalid_page_de = create_ctde(@cmtt,@cmde,@contract3)
    create_invalid_annotation(@enhanced_citation_invalid_page_de)

    @classic_citation_de = create_ctde(@cmtt,@cmde,@contract3)
  end

  def create_ctde(cmtt,cmde,c)
    ct = ContractTerm.create
    ctde = ContractTermDataElement.create(
      contract_term_id: ct.id,
      contract_id: c.id
    )
  end

  def create_valid_annotation(de1, page)
    ann1 = Annotation.new
    ann1.contract_term_data_element_id = de1.id
    ann1.contract_id = de1.contract_id 
    de1.contract_page = page
    ann1.x = 119 
    ann1.x2 = 1013 
    ann1.y = 511 
    ann1.y2 = 732
    ann1.max_width = 1114.0
    ann1.max_height = 1440.0    
    ann1.save!
    de1.save!
  end

  def create_invalid_annotation(de2)
    ann2 = Annotation.new
    ann2.contract_term_data_element_id = de2.id
    ann2.contract_id = de2.contract_id 
    ann2.save!
  end

  def generate_chksum(file_path)
    chkmsum = nil
    File.open(file_path) do |f|
      chkmsum = Digest::MD5.hexdigest(f.read)
    end
    chkmsum
  end

  def generate_image_chksum(pdf_path,page_num)
    pdf_file_name = "#{pdf_path}[#{page_num}]"
    jpeg_file = pdf_file_name + ".jpg"
    system(
      'gs',"-dFirstPage=#{page_num}","-dLastPage=#{page_num+1}",
      '-dBATCH','-dNOPAUSE','-dQUIET','-dSAFER','-sDEVICE=jpeg',
      '-r200x200', "-sOutputFile=#{jpeg_file}", pdf_path
    )    
    generate_chksum(jpeg_file)
  end

  def generate_pdf_digests
    original_pdf_path = "#{SAMPLE_DATA_FOLDER}10003.pdf"
    highlight_pdf_path = "#{SAMPLE_DATA_FOLDER}10001_highlighted.pdf"
    highlight_image_path = "#{SAMPLE_DATA_FOLDER}10001.jpeg"
    rotated_pdf_90_path = "#{SAMPLE_DATA_FOLDER}10002_rotated_90.pdf"
    rotated_pdf_270_path = "#{SAMPLE_DATA_FOLDER}10002_rotated_270.pdf"

    @highlight_pdf_digest = generate_chksum(highlight_pdf_path)
    @highlight_image_digest = generate_chksum(highlight_image_path)
    @original_pdf_digest = generate_chksum(original_pdf_path)
    @rotated_90_pdf_digest = generate_image_chksum(rotated_pdf_90_path,1)
    @rotated_270_pdf_digest = generate_image_chksum(rotated_pdf_270_path,2)
  end

  def cleanup_all_tables
    ContractModel.destroy_all
    ContractModelTermType.destroy_all
    ContractModelDataElement.destroy_all
    ContractModelTermCategory.destroy_all
    ContractModelTerm.destroy_all
    CompanyGroup.destroy_all
    ContractType.destroy_all
    ContractPoStatusType.destroy_all
    Contract.destroy_all
    ContractTerm.destroy_all
    ContractTermDataElement.destroy_all
    Annotation.destroy_all
  end
end