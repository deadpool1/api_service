require 'rails_helper'

RSpec.describe "Oauth Application configuration", :type => :request do

  context "Verify Oauth Application configurations is secured using HTTP Basic Auth " do 
    it "should return 401 unauthorized for invalid or no credentials" do
      get "/oauth/applications"
      response.status.should == 401
    end

    it "should return 200  Ok for valid credentials and lets Admin configure apps" do
      credentials = AppUser.credentials
  	  user = credentials[:user]
  	  pass = credentials[:pass]
      get "/oauth/applications", nil, {'Authorization' => "Basic " + Base64::encode64("#{user}:#{pass}")}
      response.status.should == 200
    end

    it "should the appropriate environment depending on folder name of the project" do 
    	Rails.root.stub(:to_s).and_return("/home/dummy/prod")
    	AppUser.get_env.should == 'prod'

    	Rails.root.stub(:to_s).and_return("/home/dummy/demo")
    	AppUser.get_env.should == 'demo'

    	Rails.root.stub(:to_s).and_return("/home/dummy/stage")
    	AppUser.get_env.should == 'stage'

    	Rails.root.stub(:to_s).and_return("/home/dummy/uat")
    	AppUser.get_env.should == 'uat'

    	Rails.root.stub(:to_s).and_return("/home/dummy/dev")
    	AppUser.get_env.should == 'dev'    	    	

    	Rails.root.stub(:to_s).and_return("/home/dummy/junky_project_name")
    	AppUser.get_env.should == 'dev'    	      
    end
  end
end