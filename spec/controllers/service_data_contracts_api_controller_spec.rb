require 'rails_helper'

SAMPLE_DATA_FOLDER = "#{Rails.root}/spec/data/citations/"
BASE_DOC_DIR = "#{Rails.root}/doc/raml/samples"

module BinaryAssertUtils
  def self.file_chksum(file_path)
    chkmsum = nil
    File.open(file_path) do |f|
      chkmsum = chksum(f.read)
    end
    chkmsum
  end

  def self.chksum(content)
    Digest::MD5.hexdigest(content)
  end

  def self.base64_checksum(content)
    Digest::MD5.hexdigest(Base64.decode64(content))
  end
end

describe Services::Data::V1::ContractsController do
  let(:app) {double :uid => "TEST_HEX"}
  let(:token) { double :acceptable? => true, :application => app }

  before do
    ApplicationController.any_instance.stub(:doorkeeper_token) { token }
  end  

  context "/services/data/v1/contracts/<pramata_number>" do
    it "should provide the contract level data" do
      get :show, {:pramata_number => "10001"}
      response.code.should == "200"
      actual_response = JSON.parse(response.body)

      actual_response["meta"]["pramata_number"].should == "10001"
      actual_response["data"]["type"].should == "contract"
      actual_response["included"].should == nil
    
      contract_attributes = actual_response["data"]["attributes"]

      contract_attributes["effective_date"].should == "2013-02-17"    
      contract_attributes["pramata_number"].should == "10001"    
      contract_attributes["contract_type"].should == "Master Agreement"    
      contract_attributes["document_title"].should == "Master Agreement 17-02-2013"    

      company_group_attributes = contract_attributes["customer"]

      company_group = CompanyGroup.find_by_name("Teva Pharmaceuticals USA")
      company_group_attributes["name"].should == "Teva Pharmaceuticals USA"
      company_group_attributes["id"].should == company_group.id.to_s
      company_group_attributes["links"]["self"].should == "#{Contract.new.base_url}/services/data/v1/customers/#{company_group.id}"

      Pramata::Api::ResponseValidator.assert_doc_uptodate(
      "#{BASE_DOC_DIR}/api/v1/contracts/pramata_number/200.json",
      actual_response)
    end

    it "should provide the contract related data from keyterms" do
      get :show, {:pramata_number => "10001", :include => "terms"}
      response.code.should == "200"
      actual_response = JSON.parse(response.body)

      keyterms_data = actual_response["included"]

      key_term_data = nil 
      keyterms_data.select{ |e| 
        if e["attributes"]["api_name"] == 'Contract_Sub_Class_Business_Segment'
          key_term_data = e 
          break
        end
      }

      key_term_data["type"].should == "keyterm"
      key_term_data = key_term_data["attributes"]

      key_term_data["name"].should == "Contract Sub Class (Business Segment)"
      key_term_data["terms"].size.should == 4
      key_term_data = key_term_data["terms"][0]

      key_term_data["is_amended"].should == false
      key_term_data["is_nonstandard"].should == false

      key_term_data = key_term_data["records"]
      key_term_data.size.should == 1
      key_term_data = key_term_data[0]
      key_term_data["type"].should == "data_element"

      ctde_id = key_term_data["id"]
      key_term_data = key_term_data["attributes"]
      key_term_data["data_type"].should == "file"
      key_term_data["name"].should == "Contract Sub Class"

      file_term_value = key_term_data["value"]
      file_term_value['file_type'].should == "pdf"
      file_term_value['links']['self'].should == "#{Contract.new.base_url}/services/data/v1/contracts/10001/attachment/#{ctde_id}"
      file_term_value['description'].should == "Document 1"
      citation = key_term_data["citation"]
      
      citation["type"].should == "enhanced"
      citation["attributes"]["pramata_number"].should == "10001"
      citation["attributes"]["page"].should == "1"
    end

    it "should provide the contract related data from keyterms" do
      ContractModelTermType.stub(:where).and_return([Object.new])
      get :show, {:pramata_number => "10001", :entity_api_name => "Contract_Sub_Class_Business_Segment"}
      response.code.should == "200"
      actual_response = JSON.parse(response.body)
      keyterms_data = actual_response["included"]
      keyterms_data.should == []
    end

    it "should provide the nonstandard and amended data for keyterms" do
      get :show, {:pramata_number => "10001", :include => "terms"}
      response.code.should == "200"
      actual_response = JSON.parse(response.body)

      keyterms_data = actual_response["included"]

      key_term_data = nil 
      keyterms_data.select{ |e| 
        if e["attributes"]["api_name"] == 'Contract_Class_Business_Unit'
          key_term_data = e 
          break
        end
      }

      key_term_data["type"].should == "keyterm"
      key_term_data = key_term_data["attributes"]

      key_term_data["name"].should == "Contract Class (Business Unit)"

      key_term_data["terms"].size.should == 2
      key_term_data = key_term_data["terms"][0]

      key_term_data["is_amended"].should == true
      key_term_data["is_nonstandard"].should == true

      amendment_details = key_term_data["amendment_details"]

      amendment_url = "#{Contract.new.base_url}/services/data/v1/contracts/#{@amending_contract.pontus_number}"

      amendment_details["amended_on"].should == @amending_contract.effective_date.to_s(:db)
      amendment_details["additional_info"].should == "ABC"
      amendment_details["amended_by"]["document_title"].should == @amending_contract.display_name
      amendment_details["amended_by"]["pramata_number"].should == @amending_contract.pontus_number.to_s
      amendment_details["amended_by"]["links"]["self"].should == amendment_url

      key_term_data = key_term_data["records"]
      key_term_data.size.should == 1
      key_term_data = key_term_data[0]

      key_term_data["type"].should == "data_element"
      key_term_data = key_term_data["attributes"]
      key_term_data["data_type"].should == "link_term"
      key_term_data["name"].should == "Contract Class"


      link_term_value = key_term_data["value"]
      link_term_value['pramata_number'].should == "10003"
      link_term_value['links']['self'].should == "#{Contract.new.base_url}/services/data/v1/contracts/10003"
      link_term_value['description'].should == "My Description"
      link_term_value['document_title'].should == "Participation Agreement 2012-07-14"


      citation = key_term_data["citation"]
      
      citation["type"].should == "classic"
      citation["attributes"]["pramata_number"].should == "10003"
      citation["attributes"]["page"].should == "2"
      citation["attributes"]["section"].should == "Paragraph 3"
    end

    it "should filter by contract term type related data for a contract" do
      get :show, {:pramata_number => "10001",:entity_api_name => 'Contract_Sub_Class_Business_Segment'}
      response.code.should == "200"
      actual_response = JSON.parse(response.body)

      keyterms_data = actual_response["included"]

      keyterms_data.size.should == 1

      key_term_data = keyterms_data[0]

      key_term_data["type"].should == "keyterm"
      key_term_data = key_term_data["attributes"]

      key_term_data["name"].should == "Contract Sub Class (Business Segment)"
      key_term_data["terms"].size.should == 4
      key_term_data = key_term_data["terms"][0]

      key_term_data["is_amended"].should == false
      key_term_data["is_nonstandard"].should == false

      key_term_data = key_term_data["records"]
      key_term_data.size.should == 1
      key_term_data = key_term_data[0]
      key_term_data["type"].should == "data_element"

      ctde_id = key_term_data["id"]
      key_term_data = key_term_data["attributes"]
      key_term_data["data_type"].should == "file"
      key_term_data["name"].should == "Contract Sub Class"

      file_term_value = key_term_data["value"]
      file_term_value['file_type'].should == "pdf"
      file_term_value['links']['self'].should == "#{Contract.new.base_url}/services/data/v1/contracts/10001/attachment/#{ctde_id}"
      file_term_value['description'].should == "Document 1"

      citation = key_term_data["citation"]
      
      citation["type"].should == "enhanced"
      citation["attributes"]["pramata_number"].should == "10001"
      citation["attributes"]["page"].should == "1"
    end

    it "should provide only amended terms when filter[is_amended]=true in params" do
      get :show, {:pramata_number => "10001", :filter => { :is_amended => true}, :include => "terms" }
      response.code.should == "200"
      actual_response = JSON.parse(response.body)

      keyterms_data = actual_response["included"]

      key_term_data = nil 
      keyterms_data.select{ |e| 
        if e["attributes"]["api_name"] == 'Contract_Class_Business_Unit'
          key_term_data = e 
          break
        end
      }

      key_term_data["type"].should == "keyterm"
      key_term_data = key_term_data["attributes"]

      key_term_data["name"].should == "Contract Class (Business Unit)"

      key_term_data["terms"].size.should == 2
      key_term_data["total_record_count"].should == 2
      key_term_data["page_count"].should == 2

      key_term_data = key_term_data["terms"][0]

      key_term_data["is_amended"].should == true
    end

    it "should provide only nonstandard terms when filter[is_nonstandard]=true in params" do
      get :show, {:pramata_number => "10001", :filter => { :is_nonstandard => true}, :include => "terms" }
      response.code.should == "200"
      actual_response = JSON.parse(response.body)

      keyterms_data = actual_response["included"]

      key_term_data = nil 
      keyterms_data.select{ |e| 
        if e["attributes"]["api_name"] == 'Contract_Class_Business_Unit'
          key_term_data = e 
          break
        end
      }

      key_term_data["type"].should == "keyterm"
      key_term_data = key_term_data["attributes"]

      key_term_data["name"].should == "Contract Class (Business Unit)"

      key_term_data["terms"].size.should == 1
      key_term_data = key_term_data["terms"][0]

      key_term_data["is_nonstandard"].should == true   
    end

    it "should respond 404 for invalid contract_id" do
      get :show, {:pramata_number => "-1"}
      response.code.should == "404"
      Pramata::Api::ResponseValidator.assert_doc_uptodate(
      "#{BASE_DOC_DIR}/api/v1/contracts/pramata_number/404.json",
      JSON.parse(response.body))        
    end

    it "should respond 500 for any internal server error" do
      Contract.any_instance.stub(:is_accessible_to?).and_raise("Exception")
      get :show, {:pramata_number => "10001"}
      response.code.should == "500"
    end
  end

  context "Performance testing" do 
    it 'should take less than 1 seconds for rendering whole contracts data' do
      Benchmark.realtime{
        get :show, {:pramata_number => "10001"}
      }.should < 1
    end
  end
  
  context "Pramata::Api::ContractUtil interface" do 
    it "should raise exception if class including ContractUtil doesn't implement security" do 
      class TestKlass
        include Pramata::Api::ContractUtil
      end
      expect{ TestKlass.new.is_accessible_to?(nil) }.to raise_error(Pramata::Api::ContractUtil::SecurityNotImplementedError)
      expect{ TestKlass.new.accessible_term_types }.to raise_error(Pramata::Api::ContractUtil::SecurityNotImplementedError)
    end
  end

  context "/services/data/v1/contracts/<pramata_number>/pdf" do
    
    it "should provide proper data folder without stub" do
      Contract.new.data_folder.should == "#{Rails.root}/data/contract_file/po_number/"
    end

    it "should provide the contract pdf bytes for valid  - 200 OK" do
      Contract.any_instance.stub(:data_folder).and_return(SAMPLE_DATA_FOLDER)
      get :pdf, {:pramata_number => "10001"}
      response.code.should == "200"
      actual_response = JSON.parse(response.body)

      actual_response["meta"]["pramata_number"].should == "10001"
      actual_response["meta"]["content_type"].should == "application/pdf"
      actual_response["data"]["type"].should == "contract_pdf"
    
      contract_attributes = actual_response["data"]["attributes"]

      contract_attributes["effective_date"].should == "2013-02-17"    
      contract_attributes["pramata_number"].should == "10001"    
      contract_attributes["contract_type"].should == "Master Agreement"    
      contract_attributes["document_title"].should == "Master Agreement 17-02-2013"    

      actual_chksum = BinaryAssertUtils.base64_checksum(contract_attributes["file_bytes"])
      expected_chksum = BinaryAssertUtils.file_chksum("#{SAMPLE_DATA_FOLDER}/10001.pdf")
      actual_chksum.should == expected_chksum
      Pramata::Api::ResponseValidator.assert_doc_uptodate(
      "#{BASE_DOC_DIR}/api/v1/contracts/pramata_number/pdf/200.json",
      actual_response)
    end

    it "should return 404 for Pramata Number not found" do
      get :pdf, {:pramata_number => "-1"}
      response.code.should == "404"
      actual_response = JSON.parse(response.body)

      # Pramata::Api::ResponseValidator.assert_doc_uptodate(
      # "#{BASE_DOC_DIR}/api/v1/contracts/pramata_number/200.json",
      # actual_response)
    end

    it "should return 404 for Contract file not found" do
      get :pdf, {:pramata_number => "-1"}
      response.code.should == "404"
      actual_response = JSON.parse(response.body)

      # Pramata::Api::ResponseValidator.assert_doc_uptodate(
      # "#{BASE_DOC_DIR}/api/v1/contracts/pramata_number/200.json",
      # actual_response)
    end

    it "should respond 500 for any internal server error" do
      Contract.any_instance.stub(:is_accessible_to?).and_raise("Exception")
      get :pdf, {:pramata_number => "10001"}
      response.code.should == "500"
    end
  end

  before do
    ContractModel.destroy_all
    ContractModelTermType.destroy_all
    ContractModelDataElement.destroy_all
    ContractModelTermCategory.destroy_all
    ContractModelTerm.destroy_all
    CompanyGroup.destroy_all
    ContractType.destroy_all
    ContractPoStatusType.destroy_all
    Contract.destroy_all
    ContractTerm.destroy_all
    ContractTermDataElement.destroy_all
    Annotation.destroy_all

    contract_meta_data = [
      {"model"=>"Vendor Agreements","company_group"=>"Teva Pharmaceuticals USA","contract_type"=>"Master Agreement","po_status"=>"Align - Released","pontus_number"=>10001,"display_name"=>"Master Agreement 17-02-2013","effective_date"=>"17-02-2013"},
      {"model"=>"Vendor Agreements","company_group"=>"Teva Pharmaceuticals USA","contract_type"=>"Master Agreement","po_status"=>"Deleted","pontus_number"=>10002,"display_name"=>"Master Agreement 17-09-2013","effective_date"=>"17-09-2013"},
      {"model"=>"Vendor Agreements","company_group"=>"Teva Pharmaceuticals USA","contract_type"=>"Participation Agreement","po_status"=>"Align - Released","pontus_number"=>10003,"display_name"=>"Participation Agreement 2012-07-14","effective_date"=>"17-09-2013"}
    ]

    term_types = [
      {"category"=>"Purchase Commitments", "key_term"=>"Minimum Purchase Commitment", "display_type"=>"Data Grid",  "data_element"=>"Amount in ($)",  "data_type"=>"list",  "position"=>1, "data_element_position"=>2, "is_alert_enabled" => true, "help_text"=>"Indicates the purchase ratio commitments as specified in the document. Also indicates the parameter, ratio scope, period of commit and target date."},
      {"category"=>"Purchase Commitments", "key_term"=>"Minimum Purchase Commitment", "display_type"=>"Data Grid",  "data_element"=>"Measurement Period",  "data_type"=>"list",  "position"=>1, "data_element_position"=>2, "is_alert_enabled" => true, "help_text"=>"Indicates the purchase ratio commitments as specified in the document. Also indicates the parameter, ratio scope, period of commit and target date."},
      {"category"=>"Imported Terms", "key_term"=>"Contract Class (Business Unit)", "display_type"=>"Regular","data_element"=>"Contract Class","data_type"=>"link_term",  "position"=>2, "data_element_position"=>1,  "help_text"=>"Indicates the purchase ratio commitments as specified in the document. Also indicates the parameter, ratio scope, period of commit and target date."},
      {"category"=>"Imported Terms", "key_term"=>"Contract Sub Class (Business Segment)","display_type"=>"Regular","data_element"=>"Contract Sub Class","data_type"=>"file",  "position"=>2, "data_element_position"=>2,  "help_text"=>"Indicates the purchase ratio commitments as specified in the document. Also indicates the parameter, ratio scope, period of commit and target date."}
    ]

    contract_term_data_elements = [
      {"term"=>"term2","key_term_name"=>"Contract Class (Business Unit)","data_element_name"=>"Contract Class","pramata_number"=>10001,"value"=>"10003","data_name" => "My Description","is_nonstandard"=>true,"page" => "5", "section" => "Paragraph 4"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term99","is_deleted" => true, "key_term_name"=>"Contract Class (Business Unit)","data_element_name"=>"Contract Class","pramata_number"=>10001,"value"=>"My Document.pdf","data_name" => "Document 1","is_nonstandard"=>true,"page" => "5", "section" => "Paragraph 4"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term3","key_term_name"=>"Contract Class (Business Unit)","data_element_name"=>"Contract Class","pramata_number"=>10001,"value"=>"10001","is_nonstandard"=>false,"data_name" => "My Description"},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term4","key_term_name"=>"Contract Sub Class (Business Segment)","data_element_name"=>"Contract Sub Class","pramata_number"=>10001,"value"=>"My Document.pdf","data_name" => "Document 1","is_nonstandard"=>false},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term8","key_term_name"=>"Contract Sub Class (Business Segment)","data_element_name"=>"Contract Sub Class","pramata_number"=>10001,"value"=>"My Document.pdf","data_name" => "Document 1","is_nonstandard"=>true},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term9","key_term_name"=>"Contract Sub Class (Business Segment)","data_element_name"=>"Contract Sub Class","pramata_number"=>10001,"value"=>"My Document.pdf","data_name" => "Document 1","is_nonstandard"=>false},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term10","key_term_name"=>"Contract Sub Class (Business Segment)","data_element_name"=>"Contract Sub Class","pramata_number"=>10001,"value"=>"My Document.pdf","data_name" => "Document 1","is_nonstandard"=>true},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term5","key_term_name"=>"Contract Sub Class (Business Segment)","data_element_name"=>"Contract Sub Class","pramata_number"=>10003,"value"=>"My Document.pdf","data_name" => "Document 1","is_nonstandard"=>false},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term7","key_term_name"=>"Minimum Purchase Commitment","data_element_name"=>"Amount in ($)","pramata_number"=>10001,"value"=>76665,"is_nonstandard"=>true},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
      {"term"=>"term7","key_term_name"=>"Minimum Purchase Commitment","data_element_name"=>"Measurement Period","pramata_number"=>10001,"value"=>"One-time","is_nonstandard"=>true},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ]

    amendment_data = [
      {"key_term_name"=>"Contract Class (Business Unit)","data_element_name"=>"Contract Class","pramata_number"=>10003,"value"=>"Med-dev","is_nonstandard"=>true, "amendment_qualifier" => "ABC", "amended_pramata_number" => 10001, "page" => "2", "section" => "Paragraph 3"}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
    ]

    # Test Data Preparation
    create_meta_data(contract_meta_data)
    create_term_types(term_types)    
    create_contract_term_data_elements(contract_term_data_elements)
    create_contract_term_with_amendment(amendment_data)
    ContractTermDataElement.all.each do |de|
      next if de.contract_id == Contract.where(pontus_number: 10003).first.id
      create_valid_annotation(de,1) 
    end
  end

  def create_valid_annotation(data_element, page)
    annotation = Annotation.new
    annotation.contract_term_data_element_id = data_element.id
    annotation.contract_id = data_element.contract_id 
    data_element.contract_page = page
    annotation.x = 119 
    annotation.x2 = 1013 
    annotation.y = 511 
    annotation.y2 = 732
    annotation.save!
    data_element.save!
  end

  def create_contract_term_with_amendment(amendment_data)
    [:first,:last].each do |pos|
      amendment_data.each do |ct_data|
        cmtt = ContractModelTermType.where(name: ct_data["key_term_name"]).first
        contract = Contract.where({ 
          :pontus_number => ct_data["pramata_number"]
        }).first

        @amending_contract = contract
        amended_contract = Contract.where({ 
          :pontus_number => ct_data["amended_pramata_number"]
        }).first

        ct = ContractTerm.create(
          :contract_model_id=>contract.contract_model_id,
          :contract_id=>contract.id,
          :contract_model_term_type_id=>cmtt.id,
          :is_nonstandard=>ct_data["is_nonstandard"],
          :amendment_qualifier => ct_data["amendment_qualifier"]
        )

        old_ct = ContractTerm.where( {
          :contract_id => amended_contract.id,
          :contract_model_term_type_id => cmtt.id
        }).send(pos)

        old_ct.amending_term_id = ct.id 
        old_ct.save!

        de = ContractModelDataElement.where( { 
          :name => ct_data["data_element_name"]
        }).first
        ContractTermDataElement.create(
          :contract_term_id=>ct.id,
          :contract_model_data_element_id=>de.id,
          :contract_model_id=>contract.contract_model_id,
          :contract_id=>contract.id,
          de.get_data_column=>ct_data["value"],
          :contract_page=>ct_data["page"],
          :contract_section=>ct_data["section"],
          :data_name => ct_data["data_name"]
          )
      end
    end
  end 

  def create_contract_term_data_elements(contract_term_data_elements)
    @key_term = ""
    contract_term_data_elements.each do |contract_term_data_element|
      cmtt_id = ContractModelTermType.where({ 
        :name =>  contract_term_data_element["key_term_name"]}
      ).first.id
      contract = Contract.where({ 
      :pontus_number => contract_term_data_element["pramata_number"]
      }).first
      parent = Contract.where(
        { :pontus_number => contract_term_data_element["parent_pramata_number"]}
      ).first
        if @key_term != contract_term_data_element["term"]
          @key_term = contract_term_data_element["term"]
          @ct = ContractTerm.create(:contract_model_id=>contract.contract_model_id,:contract_id=>contract.id,:contract_model_term_type_id=>cmtt_id,:is_nonstandard=>contract_term_data_element["is_nonstandard"],:is_deleted => contract_term_data_element["is_deleted"])
        end
        de = ContractModelDataElement.where( { 
          :name => contract_term_data_element["data_element_name"]}).first
        ContractTermDataElement.create(
          :contract_term_id=>@ct.id,
          :contract_model_data_element_id=>de.id,
          :contract_model_id=>contract.contract_model_id,
          :contract_id=>contract.id,
          de.get_data_column=>contract_term_data_element["value"],
          :contract_page=>contract_term_data_element["page"],
          :contract_section=>contract_term_data_element["section"],
          :data_name => contract_term_data_element["data_name"]            
        )
    end
  end

  def create_meta_data(contract_meta_data)
    contract_meta_data.each do |contract|
      company_group = CompanyGroup.where(name: contract["company_group"]).first
      company_group = CompanyGroup.create(:name => contract["company_group"]) if company_group.blank?
      contract_model = ContractModel.where(name: contract["model"]).first
      contract_model = ContractModel.create(:name => contract["model"]) if contract_model.blank?
      contract_type = ContractType.where(name: contract["contract_type"]).first
      contract_type = ContractType.create(:name => contract["contract_type"]) if contract_type.blank?
      po_status = ContractPoStatusType.where(name: contract["po_status"]).first
      po_status = ContractPoStatusType.create(:name => contract["po_status"], :is_visible_to_customer => true) if po_status.blank?
      Contract.create(
        :display_name=>contract["display_name"],
        :effective_date=>contract["effective_date"],
        :company_group_id=>company_group.id,:contract_model_id=>contract_model.id,
        :contract_type_id=>contract_type.id,:contract_po_status_type_id=>po_status.id,
        :pontus_number=> contract["pontus_number"]
      )
    end
  end

  def create_term_types(term_types)
    term_types.each do |term_type|
      @cmtc = ContractModelTermCategory.where({:name => term_type["category"]}).first
        if @cmtc.blank?
          ContractModel.all.each do |model|
            @cmtc = ContractModelTermCategory.create(:name=>term_type["category"],:contract_model_id=>model.id,:position=>term_type["position"])
          end
        end
        @cmtt = ContractModelTermType.where(name: term_type["key_term"]).first
        if @cmtt.blank?
          @cmtt = ContractModelTermType.create(:name=>term_type["key_term"],:contract_model_term_category_id=>@cmtc.id,:display_type=>term_type["display_type"],:is_alert_enabled => term_type["is_alert_enabled"])
          if term_type["is_alert_enabled"]
            @cmtt_alert_enabled = @cmtt 
          end
          ContractModelTermCategory.where({ :name => term_type["category"]}).each do |cmtc|
            ContractModelTerm.create(:contract_model_term_category_id=>cmtc.id,:contract_model_id=>cmtc.contract_model_id,:contract_model_term_type_id=>@cmtt.id,:position=>term_type["data_element_position"],:customer_term_tip=>term_type["help_text"])
          end
        end
        data_elements = []
        @cmtt.contract_model_data_elements.collect{|cmde| data_elements<<cmde.name}
        if !data_elements.include?"#{term_type["data_element"]}"
          ContractModelDataElement.create(:name=>term_type["data_element"],:contract_model_term_type_id=>@cmtt.id,:data_type=>term_type["data_type"])
        end
    end
  end
end