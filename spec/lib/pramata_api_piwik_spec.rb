require 'rails_helper'

TOKEN_AUTH = "46588de931a069008c12601130e10950"
PIWIK_URL = "https://analytics-stage.pramata.com"
describe "Pramata::Api::Piwik" do

  before do
    ApplicationController.any_instance.stub(:doorkeeper_token) { token }
  	Delayed::Worker.delay_jobs = false    
  end  

  context "validate the piwik tracking" do 

    def actions_count 
      parameters = {
        module: "API",
        idSite: Pramata::Api::Analytics.piwik_site_id,
        period: "day",
        format: "json",
        method: "API.get",
        date: "today",
        token_auth: TOKEN_AUTH
      }

      piwik_url = Pramata::Api::Analytics.piwik_server_url
      report = JSON.parse((RestClient.get piwik_url, {params: parameters}).body)
      p " REPORT OUTPUT"
      p report
      actions =  report["nb_actions"]
    end

    it "should increment the count of event once tracking is triggered" do
      actions_before =  actions_count
      Pramata::Api::Analytics.track_page_view(
        "pramata.com/home","Category","1243"
      )
      Pramata::Api::Analytics.track_event(
        "pramata.com/home", "1243","testconsole","testconsoleaction","NewTrack"
      )
      actions_after = actions_count
      actions_after.should == (actions_before+1)
    end
   end
end