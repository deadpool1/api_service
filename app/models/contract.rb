require "#{Rails.root}/lib/pramata"
class Contract < NavigatorModel
  belongs_to :contract_po_status_type
  belongs_to :contract_type
  belongs_to :company_group

  has_many :contract_terms
  has_many :contract_term_data_elements

  include ::Pramata::Api::ContractUtil

  # Implementing the security as part of contract with ContractUtil
  def accessible_term_types
    ContractModelTermType.find_by_sql([
        "select cmtt.id as id, cmtt.name as name,
      cmtt.api_name as api_name,
      cmtt.display_type, is_alert_enabled
      from
      contract_model_term_types cmtt inner join contract_model_terms cmt on
      cmtt.id=cmt.contract_model_term_type_id where
      cmt.contract_model_id=:contract_model_id order by
      cmt.position",
        { :contract_model_id => contract_model_id }
      ])
  end

  def data_folder 
    "#{Rails.root}/data/contract_file/po_number/"
  end

  def pdf_as_json 
    {
      meta: {
        content_type: "application/pdf",
        pramata_number: pontus_number.to_s
      },
      data: {
        id: id.to_s,
        type: "contract_pdf",
        attributes: {
          file_bytes: pdf_base64,
          pramata_number: pontus_number.to_s,
          contract_type: (contract_type.name rescue ""),
          document_title: display_name.to_s,
          effective_date: effective_date.iso8601
        }
      }
    }
  end

  def pdf_path 
    file_path = "#{data_folder}#{pontus_number}.pdf"
  end
   
  def pdf_base64 
    Base64.encode64(File.read(pdf_path))
  end
end