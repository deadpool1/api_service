class AppUser < NavigatorModel
  
  def self.authenticate(u,p)
    is_authenticated=false
    user, pass = credentials[:user], credentials[:pass]
    is_authenticated = true if u == user && p == pass
    return is_authenticated
  end

  def self.credentials
  	{
  	  user: APP_CONFIG[:api_basic_auth][get_env]["username"],
      pass: APP_CONFIG[:api_basic_auth][get_env]["password"]
    }
  end

  def self.get_env
      env_entry = ""
      rails_root = Rails.root.to_s
      if rails_root =~ /\/prod$/
        env_entry = "prod"
      elsif rails_root =~ /\/demo$/
        env_entry = "demo"
      elsif rails_root =~ /\/stage$/
        env_entry = "stage"
      elsif rails_root =~ /\/uat$/
        env_entry = "uat"
      else
        env_entry = "dev"
      end
      env_entry
   end

end

