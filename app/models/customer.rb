class Customer < NavigatorModel
  def data(role_id,options={})
    customers_json = {
      meta:{
        id: id.to_s
      },
      data:{
        id: id.to_s,
        type: "customer",
        attributes: {}
      },
      links:{
        self:"#{$tenant.api_service_url}/services/data/v1/customers/"+id.to_s,
      }
    }    

    Rails.logger.error(customers_json.inspect)
    CustomerEntityConfiguration.field_names.each do |field_name|
      customers_json[:data][:attributes][field_name] = send(field_name)
    end

    return customers_json
  end
end

