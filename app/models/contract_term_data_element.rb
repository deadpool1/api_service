class ContractTermDataElement < NavigatorModel
	belongs_to :contract
	belongs_to :contract_term
	belongs_to :contract_model_data_element
end