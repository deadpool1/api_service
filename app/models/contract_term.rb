class ContractTerm < NavigatorModel
	belongs_to :contract
	belongs_to :contract_model_term_type
	has_many :contract_term_data_elements
end