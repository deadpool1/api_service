class ContractModelDataElement < NavigatorModel
  belongs_to :contract_model_term_type
  def get_data_column
    case self.data_type
    when /string|list|file|string_search|string_autocomplete/
      return "data_string"
    when /integer|link_term/
      return "data_integer"
    when /decimal-currency|decimal-percentage/
      return "data_decimal"
    when "date"
      return "data_date"
    when "boolean"
      return "data_boolean"
    else
      return "data_string"
    end
  end
end