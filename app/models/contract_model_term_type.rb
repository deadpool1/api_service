class ContractModelTermType < NavigatorModel
  has_many :contract_model_data_elements
  has_many :contract_terms

  after_create :generate_api_name

  def generate_api_name
    if self.api_name.blank?
      str = self.name.split(/[^A-Za-z0-9]/).map(&:capitalize).join("_").gsub(/[_]+/,'_')
      str = str.gsub(/^(.{95,}?).*$/m,'\1')
      self.api_name = "#{str}"
      self.save!
    end
  end
end