class CustomerEntityConfiguration < NavigatorModel 
	def self.field_names
		@field_names = @field_names || where("is_enabled_for_display=1").collect{|c| c.field_name}
	end
end