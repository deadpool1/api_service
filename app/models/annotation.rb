class Annotation < NavigatorModel
  belongs_to :contract_term_data_element
  belongs_to :contract
end