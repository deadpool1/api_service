class SharingRule < NavigatorModel 
  def self.has_access?(object,role_id)
    exists?( 
      object_type: object.class, 
      object_id: object.id, 
      role_id: role_id
    )
  end
end