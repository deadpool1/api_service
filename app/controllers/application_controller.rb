class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def respond_not_ready
    render json: {
      :errors => [
        {
          :status => "502",
          :source => { "pointer" => "/not_yet_ready" },
          :title =>  "Not Yet Implemented",
          :detail => "API is still under development"
        }
      ]
    }, status: 502
  end

  def respond_internal_error 
    render :json => {
      :errors => [
        {
          :status => "500",
          :source => { "pointer" => "/contracts/pramata_number" },
          :title =>  "Internal Server Error",
          :detail => "Please contact support@pramata.com with the URL and Pramata Number of this request"
        }
      ]
    }, :status => 500
  end

  def respond_not_found(pointer="/record_id", title="Invalid Record Id", detail="No matching records found for id: #{params[:id]}")
    render :json => {
      :errors => [
        {
          :status => "404",
          :source => { "pointer" => pointer },
          :title =>  title,
          :detail => detail
        }
      ]
    }.to_json, :status => 404
  end

  def track_request(event_options = { category: request.original_fullpath, action: "GET", name: params[:action] })
    if event_options.present?
      Pramata::Api::Analytics.track_event(
        request.original_url,
        doorkeeper_token.application.uid,
        event_options[:category],
        event_options[:action],
        event_options[:name]
      )
    end
    Pramata::Api::Analytics.track_page_view(
      request.original_url,
      request.original_fullpath,
      doorkeeper_token.application.uid
    )    
  end
end
