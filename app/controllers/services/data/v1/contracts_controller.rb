class Services::Data::V1::ContractsController < ApplicationController
  def index 
    contracts_json = {
      data: [],
      meta: {}
    }    
    contracts = []
    if params[:customer_id] 
      customer = Customer.find(params[:customer_id])
      contracts = Contract.find_by_sql([
        "select c.* from contracts c 
        inner join customer_contract_associations cca 
        where cca.customer_id = ?
        and cca.contract_id = c.id",
        params[:customer_id]
      ])
      contracts_json[:meta][:total_records] = contracts.size.to_s
      contracts_json[:meta][:current_records] = contracts.size.to_s
      contracts_json[:meta][:customer] = {}
      contracts_json[:meta][:customer][:name] = customer.primary_name
      contracts_json[:meta][:customer][:id] = customer.id
    else
      contracts = Contract.all.limit(2000)
      contracts_json[:meta][:total_records] = Contract.count    
      contracts_json[:meta][:current_records] = 2000    
    end
    contracts.each do |contract|
      if SharingRule.has_access?(contract,$current_role_id)
        contracts_json[:data].push(contract.data(nil)[:data])
      end
    end
    render json: contracts_json
  rescue Exception => e   
    Rails.logger.error("#{e.class} - #{e.message} - #{e.backtrace}")
    respond_internal_error    
  end

  def show 
    c = Contract.find_by_pontus_number(params[:pramata_number])
    if c.present? and SharingRule.has_access?(c,$current_role_id)
      options = {}
      options[:term_type] = params[:entity_api_name]
      options[:filter] = params[:filter]
      options[:include] = params[:include]
      render :json => c.data(nil, options)
    else
      respond_not_found(
        "/contracts/pramata_number",
        "Invalid Pramata Number",
        "No matching Contracts found in the repository for the given Pramata Number"
      )
    end
  rescue Exception => e   
    Rails.logger.error("#{e.class} - #{e.message} - #{e.backtrace}")
    respond_internal_error
  end

  def categories
    c = Contract.find_by_pontus_number(params[:pramata_number])
    if c.present? and SharingRule.has_access?(c,$current_role_id)
      render :json => c.categories
    else
      respond_not_found(
        "/contracts/pramata_number/categories",
        "Invalid Pramata Number",
        "No matching Contract found in the repository for the given Pramata Number"
      )
    end
  rescue Exception => e    
    Rails.logger.error("#{e.class} - #{e.message} - #{e.backtrace}")
    respond_internal_error
  end

  def pdf 
    c = Contract.find_by_pontus_number(params[:pramata_number])
    if c.present? and SharingRule.has_access?(c,$current_role_id)
      render :json => c.pdf_as_json
    else
      respond_not_found(
        "/contracts/pramata_number",
        "Invalid Pramata Number",
        "No matching Contract PDF found in the repository for the given Pramata Number"
      )
    end
  rescue Exception => e    
    Rails.logger.error("#{e.class} - #{e.message} - #{e.backtrace}")
    respond_internal_error
  end

  def pdf_file
    c = Contract.find_by_pontus_number(params[:pramata_number])
    if c.present? and SharingRule.has_access?(c,$current_role_id)
      send_file c.pdf_path, {
        :filename => c.pontus_number.to_s+".pdf", 
        :type => 'application/pdf', 
        :disposition => 'inline'
      }  
    else
      respond_not_found(
        "/contracts/pramata_number",
        "Invalid Pramata Number",
        "No matching Contract PDF found in the repository for the given Pramata Number"
      )
    end
  rescue Exception => e    
    Rails.logger.error("#{e.class} - #{e.message} - #{e.backtrace}")
    respond_internal_error
  end  
end