class Services::Data::V1::CustomersController < ApplicationController

  CUSTOMER_PAGE_LIMIT = 30
  def index
    Rails.logger.error("page: #{params[:page].inspect}")
    page = params[:page] || {}
    limit = (page["limit"] || CUSTOMER_PAGE_LIMIT).to_i
    offset = (page["offset"] || 0).to_i
    customers = Customer.all.limit(limit).offset(offset)
    customers_json = {
      meta: {
        total_records: Customer.count,
        current_records: customers.size,
        links: {
          self: "#{$tenant.api_service_url}/services/data/v1/customers?page[limit]=#{limit}&page[offset]=#{offset}"
        }
      },
      data: []
    }

    unless Customer.count.eql?(customers.size+(limit*offset))
      customers_json[:meta][:links][:next] = "#{$tenant.api_service_url}/services/data/v1/customers?page[limit]=#{limit}&page[offset]=#{offset+limit}"
    end

    if (offset-limit) >= 0
      customers_json[:meta][:links][:prev] = "#{$tenant.api_service_url}/services/data/v1/customers?page[limit]=#{limit}&page[offset]=#{offset-limit}"
    end

    customers.each do |customer|
      customers_json[:data].push customer.data($role_id, {})
    end
    sleep 1
    render json: customers_json
  rescue Exception => e 
    Rails.logger.error("#{e.class} - #{e.message} - #{e.backtrace}") 
    respond_internal_error   
  end

  def show 
    customer = Customer.find(params[:id])
    if customer.present? #and SharingRule.has_access?(c,$current_role_id)
      options = {}
      options[:filter] = params[:filter]
      options[:include] = params[:include]
      render :json => customer.data($role_id, options)
    else
      respond_not_found(
        "/customers/customer_id",
        "Invalid Customer Identifier",
        "No matching customers found in the repository for the given customer_id"
      )
    end
  rescue Exception => e   
    Rails.logger.error(e)
    respond_internal_error
  end
end