module Pramata
  module Citation
    class HighlightGenerationError < StandardError 
    end

    class Highlighter 
      DEFAULT_SCALE_WIDTH = 600.0
      STEPS = {
        "1" => "step_1",
        "2" => "step_2",
        "3" => "step_3",
        "4" => "step_4"
      }

      def initialize(ctde, scale_width = DEFAULT_SCALE_WIDTH, audit = nil)
        @ctde = ctde
        @scale_width = scale_width || DEFAULT_SCALE_WIDTH
        @performance_audit = audit
        @folder_name = "#{Time.now.to_i}_#{rand(1000)}"
        create_dir processing_dir        
      end

      def log_time(start_time,end_time,step)
        if !STEPS.values.include?(step) or @performance_audit.nil?
          return 
        end
        @performance_audit.update_attribute(step,(end_time-start_time))
      end

      def processing_dir
        "#{Rails.root}/tmp/citations/#{@folder_name}"
      end

      def ctde 
      	@ctde 
      end

      private 

      def annotation
        @ann = @ann || Annotation.find_by_contract_term_data_element_id(ctde.id)
      end
      
      def max_png_width 
        annotation.max_width
      end 

      def max_png_height 
        annotation.max_height
      end

      def data_folder 
        "#{Rails.root.to_s}/data/contract_file/po_number/"
      end

      def create_dir dir
        if File.directory?(dir)
          FileUtils.rm_rf Dir.glob("#{dir}/*")
        else
          FileUtils.mkdir_p(dir)
        end
      end

      def pdfbox_bin
        "#{Rails.root}/bin/pdfbox-app-2.0.2.jar"
      end

      def validate_params(page_num)
        validate_coordinates = (annotation.x && annotation.y && annotation.x2 && annotation.y2 && page_num)
        if !validate_coordinates
          raise PreviewGenerationError, annotation.inspect
        end
      rescue Exception => e
        raise PreviewGenerationError, annotation.inspect
      end
    end
  end 
end