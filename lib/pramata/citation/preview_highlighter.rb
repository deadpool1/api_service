require 'RMagick'

module Pramata
  module Citation
    class PreviewGenerationError < HighlightGenerationError
    end

    class PreviewHighlighter < Highlighter
      HIGHLIGHT_IMAGE_OPACITY = 0.2    
      IMAGE_STROKE_WIDTH = 1
      IMAGE_YELLOW_COLOR = 'yellow'
      OUTLINE_RECTANGLE_BORDER = 200
      OUTLINE_RECTANGLE_WIDTH_OFFSET = 100
      MAX_IMAGE_WIDTH = 1654
      ORIGIN_X = 0

      # STEP 1: Convert particular page from PDF to Image using Ghostscript
      # STEP 2: Draw a yellow rectangle around the marked annotation using RMagick
      # STEP 3: Crop the image to a rectangle using RMagick
      # STEP 4: Scaleup/down using RMagick

      def generate
        contract = ctde.contract
        pramata_number = contract.pontus_number   

        source_pdf_path = "#{data_folder}#{pramata_number}.pdf"
        page_num = ctde.contract_page.to_i
        
        output_image = "#{processing_dir}/#{pramata_number}.jpeg"
        convert_to_image(source_pdf_path, page_num, output_image)
        highlight_in_image(output_image)

        output_image
      end

      private 

      def width_ratio 
        @source_image.columns/max_png_width
      end 

      def height_ratio 
        @source_image.rows/max_png_height
      end 

      def highlight_in_image(source_image)
        images = Magick::Image.read(source_image)
        image = images[0]
        @source_image = image
        scaled_x = annotation.x*width_ratio
        scaled_y = annotation.y*height_ratio

        scaled_x2 = annotation.x2*width_ratio
        scaled_y2 = annotation.y2*height_ratio

        construct_rectangle(scaled_x, scaled_y, scaled_x2, scaled_y2, image)

        outer_y = scaled_y - OUTLINE_RECTANGLE_WIDTH_OFFSET

        width = (scaled_x2 - scaled_x) + OUTLINE_RECTANGLE_BORDER
        height = (scaled_y2 - scaled_y) + OUTLINE_RECTANGLE_BORDER

        start_time = Time.now
        snip = image.crop!(ORIGIN_X,outer_y,image.columns,height)
        log_time(start_time,Time.now,STEPS["3"])

        start_time = Time.now
        scale_factor = @scale_width/image.columns
        snip = snip.scale(scale_factor)
        snip.write(source_image)
        log_time(start_time,Time.now,STEPS["4"])
      end

      def construct_rectangle(scaled_x, scaled_y, scaled_x2, scaled_y2, image)
        start_time = Time.now
        gc = Magick::Draw.new
        gc.fill(IMAGE_YELLOW_COLOR)
        gc.fill_opacity(HIGHLIGHT_IMAGE_OPACITY)
        gc.stroke(IMAGE_YELLOW_COLOR)
        gc.stroke_width(IMAGE_STROKE_WIDTH)
        gc.rectangle(scaled_x, scaled_y, scaled_x2, scaled_y2)
        gc.draw(image)
        log_time(start_time,Time.now,STEPS["2"])        
      end

      def ghost_script 
        # "#{Rails.root}/bin/gs-920-linux_x86_64"
        "gs"
      end

      def convert_to_image(source_pdf_path, page_num, output_image)
        start_time = Time.now
        validate_params(page_num)
        system(
          ghost_script,"-dFirstPage=#{page_num}","-dLastPage=#{page_num+1}",
          '-dBATCH','-dNOPAUSE','-dQUIET','-dSAFER','-sDEVICE=jpeg',
          '-r200x200', "-sOutputFile=#{output_image}", source_pdf_path
        )
        log_time(start_time,Time.now,STEPS["1"])
      end
    end
  end 
end