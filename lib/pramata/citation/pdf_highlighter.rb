require 'prawn'
require 'pdfinfo'

module Pramata
  module Citation
    class PdfGenerationError < HighlightGenerationError
    end

    class PdfHighlighter < Highlighter

      # PDF standard Margin around X and Y axis
      PDF_X_OFFSET = 35 
      PDF_Y_OFFSET = 35

      # Color code of PDF CMYK and doesn't support RGB or HTML color codes
      CMYK_YELLOW_COLOR = [0,0,100,0] 

      # 1.0 for full Opaque
      # 0.0 for full Transparent
      PDF_HIGHLIGHT_OPACITY = 0.3

      # STEP 1: Read PDF points form metadata using pdfinfo 
      # STEP 2: Create highlight PDF with prawn
      # STEP 3: Rotate PDF using pdf90
      # STEP 4: Merge highlight PDF and original PDF using pdfbox.jar
  
      def generate
        contract = ctde.contract
        pramata_number = contract.pontus_number

        original_pdf = "#{data_folder}#{pramata_number}.pdf"
        output_file = original_pdf

        if annotation.present?
          output_file = draw_with_coordinates(original_pdf, ctde.contract_page)
        end
        output_file
      end

      private

      # Key for converting Saved Image pixels to PDF Points
      def width_ratio
        @pdf_coordinates.width/max_png_width
      end

      def height_ratio
        @pdf_coordinates.height/max_png_height
      end

      def metadata_from_pdfinfo(pdf_path, page_num)
        Open3.capture2('pdfinfo', '-f', page_num.to_s, '-l', page_num.to_s, pdf_path)
      end

      def metadata(pdf_path,page_num)
        output, st = metadata_from_pdfinfo(pdf_path, page_num)
        output = output.encode!('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
        width_height_regex = /^Page\s+\d+\s+size:\s+([-+]?[0-9]*\.?[0-9]*)\sx\s([-+]?[0-9]*\.?[0-9]*).*$/
        rotation_angle_regex = /^Page\s+\d+\s+rot:\s+(\d+).*$/

        rotation_angle = rotation_angle_regex.match(output).captures[0].to_i rescue rotation(pdf_path,page_num)
        width, height = width_height_regex.match(output).captures.map(&:to_i)
        
        # Swap the Width and Height depending upon Page rotation Angle
        # 0* => No Swap (W' = W; H' = H)
        # 90* => Swap (W' = H; H' = W)
        # 180* => No Swap (W' = W; H' = H)
        # 270 => Swap (W' = H; H' = W)
    
        if (rotation_angle/90)%2 == 1
          width, height = height, width
        end

        {
          width: width,
          height: height,
          rotation: rotation_angle
        }
      end

      def rotation(pdf_path,page_num)
        tmp_file = "#{processing_dir}/#{page_num}.pdf"
        system('pdftk', pdf_path, 'cat', page_num.to_s, 'output', tmp_file)        
        pdf_meta = Origami::PDF.read tmp_file, lazy: true  # loads only metadata
        pdf_meta.get_page(1).Rotate.to_i
      rescue Exception => e 
        Rails.logger.info(e)
        return 0
      end

      def set_current_pdf_points(pdf_path, page_num)
        start_time = Time.now 
        pdf_info = metadata(pdf_path, page_num)
        @pdf_coordinates = OpenStruct.new pdf_info
        log_time(start_time,Time.now,STEPS["1"])
      end

      def draw_with_coordinates(original_pdf,page_num)

        validate_params(page_num)
        set_current_pdf_points(original_pdf,page_num)
        highlight_pdf = create_highlight_pdf(processing_dir)

        merged_pdf = merge_pdfs(original_pdf, highlight_pdf, page_num,
          processing_dir)
        merged_pdf
      end

      def merge_pdfs(pdf_1, pdf_2, page, dir)
        start_time = Time.now        
        merged_pdf = "#{dir}/#{ctde.id}.pdf"
        system('java', '-jar', pdfbox_bin,
          'OverlayPDF', pdf_1,
          '-position', 'foreground', 
          '-page', page.to_s, 
          pdf_2, merged_pdf)
        log_time(start_time,Time.now,STEPS["4"])        
        return merged_pdf
      end

      def rotate_pdf(pdf_path, angle)
        start_time = Time.now
        if angle > 0 
          rotations = (angle/90)
          rotated_pdf = pdf_path+"rotated.pdf"
          rotations.times do 
            system('pdf90', pdf_path, '--outfile', rotated_pdf)
            system('mv',rotated_pdf,pdf_path)
          end
        end
        log_time(start_time,Time.now,STEPS["3"])                        
      end

      def create_highlight_pdf(output_dir)
        start_time = Time.now        
        new_pdf = "#{output_dir}/highlight.pdf"

        transformed_y = @pdf_coordinates.height - annotation.y*height_ratio
        transformed_x = annotation.x*width_ratio

        h = (annotation.y2-annotation.y)*height_ratio
        w = annotation.x2*width_ratio - transformed_x

        Prawn::Document.generate(new_pdf, 
          :page_size => [@pdf_coordinates.width, @pdf_coordinates.height]) do |pdf|
          pdf.transparent(PDF_HIGHLIGHT_OPACITY) do
            pdf.fill_color(*CMYK_YELLOW_COLOR)
            pdf.fill_rectangle [transformed_x - PDF_X_OFFSET, transformed_y - PDF_Y_OFFSET], w, h
          end
        end
        log_time(start_time,Time.now,STEPS["2"])        

        rotate_pdf(new_pdf, @pdf_coordinates.rotation)
        new_pdf        
      end
    end
  end 
end