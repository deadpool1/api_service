module Pramata
  module Api
    module ResponseValidator 
      def self.assert_doc_uptodate(expected_json_file, actual_response)
        schema = JSON::SchemaGenerator.generate( "Test", 
          File.read(expected_json_file), 
          {:schema_version => 'draft4'}
        )

        assert_jsonapi_compliant(actual_response)      
        JSON::Validator.validate!(schema, actual_response)  
      end

      def self.assert_jsonapi_compliant(actual_response)
        JsonApi::Validator.object(actual_response).size.should == 0
      end
    end
  end
end