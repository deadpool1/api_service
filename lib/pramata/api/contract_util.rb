module Pramata
  module Api
    # This module provides common API wrappers for Contract
    # 1. Provides jsonapi.org based json response for key terms and metadata
    # 2. Categories for all the keyterms under a given Contract
    # 
    # Check:
    # 1. Include Customer level Security 
    # 2. Benchmark term cache usage
    module ContractUtil
      class SecurityNotImplementedError < StandardError
      end

      META_TERM_TYPES = [
        {
          "Customer" => {
            :column_name => "company_group.name",
            :data_type => "string",
            :display_type => "Regular"
          }
        },
        {
          "Effective Date" => {
            :column_name => "effective_date",
            :data_type => "date",
            :display_type => "Regular"
          }
        },
        {
          "Contract Type" => {
            :column_name => "contract_type.name",
            :data_type => "string",
            :display_type => "Regular"
          }
        },
        {
          "Pramata Number" => {
            :column_name => "pontus_number",
            :data_type => "integer",
            :display_type => "Regular"
          }
        },
        {
          "Document Title" => {
            :column_name => "display_name",
            :data_type => "string",
            :display_type => "Regular"
          }
        }
      ]
      module CitationType
        ENHANCED = "enhanced"
        CLASSIC = "classic"
      end

      module DataElementType
        LINK_TERM = "link_term"
        FILE_TERM = "file"
      end

      def data(app_user, options = {})
        json_data = meta
        @app_user = app_user
        if options and ((options[:include] and options[:include] == 'terms') or options[:term_type])
          json_data[:included] = []
          @all_contract_terms = all_contract_terms
          @all_model_des = all_model_des
          @all_annotations = {}
          @all_annotations = all_annotations

          populate_terms_data(json_data, app_user, options)
        end
        json_data
      end

      def all_annotations
        annotations = Annotation.find_by_sql(["select contract_term_data_element_id,x,x2,y,y2 from annotations where contract_id=?",id])
        result = {}
        annotations.each do |annotation|
          result[annotation.contract_term_data_element_id] = annotation
        end
        return result
      end

      def accessible_term_types
        raise SecurityNotImplementedError
      end

      def tenant_suffix_params 
        # "?tenant_name=#{$tenant.name}"
        ""
      end

      def base_url
        "https://#{$tenant.name}.pramata.com"
        # APP_CONFIG[:api_config][AppUser.get_env]["base_url"]
        # $tenant.api_service_url
      end

      def all_model_des
        model_des = {}
        ContractModelDataElement.all.each do |cmde|
          model_des[cmde.id] = cmde 
        end
        model_des
      end

      def all_contract_terms 
        contract_terms.where("amend_contract_id is null and
          amended_term_id is null and
          (is_deleted is null or is_deleted=0)")
      end

      def filtered_term_types(term_type_name)
        term_type = nil
        if term_type_name.present?
          term_type = ContractModelTermType.where(api_name: term_type_name).first
        end
        term_types = accessible_term_types
        if term_type.present? 
          if term_types.include?(term_type) 
            term_types = [term_type]
          else
            term_types = []
          end
        end
        return term_types
      end

      def populate_terms_data(json_data, app_user, options)
        term_type = options[:term_type]
        filtered_term_types(term_type).each do |term_type|
          term_array = term_type_data(term_type, app_user, options)
          unless term_array[:attributes][:terms].blank?
            json_data[:included] << term_array 
          end
        end 
      end

      def term_type_data(term_type, app_user, options)
        terms, record_count, total_record_count = terms_data(term_type, app_user, options)
        {
          :type => "keyterm",
          :id => term_type.api_name,
          :attributes => {
            :api_name => term_type.api_name,
            :name => term_type.name,
            :id => term_type.id,
            :contract_id => id,
            :terms => terms,
            :total_record_count => total_record_count,
            :page_count => record_count
          }
        }
      end

      def meta
        {
          :meta => {
            :id => id.to_s,
            :pramata_number => pontus_number.to_s
          },
          :data => contract_data,
          :links => {
            :self => "#{base_url}/services/data/v1/contracts/#{pontus_number}#{tenant_suffix_params}",
            :pdf => "#{base_url}/services/data/v1/contracts/#{pontus_number}/pdf#{tenant_suffix_params}"
          }
        }
      end 

      def contract_data
        {
          :type => "contract",
          :id => id.to_s,
          :attributes => {
            :pramata_number => pontus_number.to_s,
            :document_title =>  display_name.to_s, 
            :effective_date => (effective_date.to_s.blank? ? nil : effective_date.iso8601),
            :contract_type => (contract_type.name rescue nil),
            :parent_id => parent_id
          }
        }
      end

      def company_group_data
        cg = company_group
        {
          :id => (cg.id.to_s rescue nil),
          :name => (cg.name rescue nil),
          :links => { 
            :self =>  "#{base_url}/services/data/v1/customers/#{cg.id}#{tenant_suffix_params}"
          }
        }
      end

      def ordered_data_elements(ct)
        ContractTermDataElement.find_by_sql(
          "select ctde.* from contract_term_data_elements ctde 
          INNER JOIN contract_model_data_elements cmde
          ON cmde.id = ctde.contract_model_data_element_id
          and ctde.contract_term_id = #{ct.id}
          order by cmde.position asc"
        )
      end

      def filtered_contract_terms(term_type, options)
        result = []
        record_count = 0
        filter = options[:filter]
        @all_contract_terms.each do |term|

          if filter.present? and filter["is_amended"].to_s == "true" and term.amending_term_id.blank?
            next 
          end

          if filter.present? and filter["is_nonstandard"].to_s == "true" and term.is_nonstandard.blank?
            next
          end

          if term_type.id == term.contract_model_term_type_id
            result << term
            record_count += 1
          end
        end

        total_count = 0
        @all_contract_terms.each do |term|
          if term_type.id == term.contract_model_term_type_id
            if filter.present? and filter["is_amended"].to_s == "true" 
              if term.amending_term_id.present?
                total_count += 1
              end
              next 
            end

            if filter.present? and filter["is_nonstandard"].to_s == "true" 
              if term.is_nonstandard.present?
                total_count += 1
              end
              next
            end

            total_count += 1
          end
        end

        [result,record_count,total_count]
      end

      def terms_data(term_type, app_user, options)
        contract_terms_data = []
        result, record_count, total_count = filtered_contract_terms(term_type, options)

        result.each do |ct|
          amending_term = nil
          citation_pramata_number = pontus_number

          ct_data = {
            :id => ct.id,
            :is_nonstandard => (ct.is_nonstandard == true),
            :is_amended => false
          }
          
          data_elements = []
          cit_title, cit_p_no, amending_term = populate_amendment_data(ct,ct_data)
          ordered_data_elements(ct).each do |ctde|
            citation_ctde = get_citation_de(ctde, amending_term)
            data_elements << collect_des(ctde, citation_ctde, cit_p_no, cit_title)
          end

          ct_data[:records] = data_elements
          contract_terms_data << ct_data
        end

        [contract_terms_data, record_count, total_count]
      end

      def populate_amendment_data(ct,ct_data)
        cit_p_no = nil
        cit_title = nil
        amending_term_id = ct.amending_term_id
        amending_term = nil
        unless amending_term_id.blank?
          ct_data[:is_amended] = true
          amending_term = ContractTerm.where(id: ct.amending_term_id).first
          amending_contract = amending_term.contract
          cit_p_no = amending_contract.pontus_number
          cit_title = amending_contract.display_name
          ct_data[:amendment_details] = {
            :amended_by => {
              :document_title => amending_contract.display_name,
              :pramata_number => cit_p_no.to_s,
              :links => {
                :self => "#{base_url}/services/data/v1/contracts/#{cit_p_no}#{tenant_suffix_params}"
              }
            },
            :amended_on => amending_contract.effective_date.to_s(:db),
            :additional_info => amending_term.amendment_qualifier
          }
        else 
          contract = ct.contract
          cit_p_no = contract.pontus_number
          cit_title = contract.display_name
        end
        [cit_title, cit_p_no, amending_term]
      end

      def get_citation_de(ctde,amending_term) 
        if amending_term.present?
          ContractTermDataElement.where([
              'contract_term_id=? and contract_model_data_element_id=?',
              amending_term.id, 
              ctde.contract_model_data_element_id
            ]).order(:contract_id).first
        else
          ctde 
        end
      end

      def collect_des(ctde, citation_ctde, pramata_number, contract_title)
        cmde = @all_model_des[ctde.contract_model_data_element_id]
        {
          :type => "data_element",
          :id => ctde.id.to_s,
          :attributes => {
            :data_type => cmde.data_type,
            :api_name => cmde.api_name,
            :name => cmde.name,
            :value => get_data_from_de(ctde,cmde),
            :citation => citation_for(ctde, citation_ctde, pramata_number, contract_title)
          }
        }
      end

      def citation_for(ctde, citation_ctde, pramata_number, contract_title)
        annotation = @all_annotations[citation_ctde.id]
        if (citation_ctde.contract_id != id)
          annotation = Annotation.where(contract_term_data_element_id: citation_ctde.id).first
        end
        if annotation and annotation.x and annotation.x2 and annotation.y and annotation.y2
          {
            :type => CitationType::ENHANCED,
            :attributes => {
              :page => citation_ctde.contract_page.to_s,
              :pramata_number => pramata_number.to_s,
              :document_title => contract_title,
              :preview_dimension => {
                :width => annotation.x2 - annotation.x,
                :height => annotation.y2 - annotation.y
              }
            },
            :links => {
              :self => "#{base_url}/services/data/v1/contracts/#{pramata_number}/terms/#{citation_ctde.contract_term_id}/des/#{citation_ctde.id}/citation/pdf#{tenant_suffix_params}",
              :preview => "#{base_url}/services/core/api/v1/contracts/#{pramata_number}/terms/#{citation_ctde.contract_term_id}/des/#{citation_ctde.id}/citation/preview#{tenant_suffix_params}"
            }
          }    
        else
          {
            :type => CitationType::CLASSIC,
            :attributes => {
              :section => citation_ctde.contract_section.to_s,
              :page => citation_ctde.contract_page.to_s,
              :pramata_number => pramata_number.to_s,
              :document_title => contract_title                
            },
            :links => {
              :self => "#{base_url}/services/data/v1/contracts/#{pramata_number}/terms/#{citation_ctde.contract_term_id}/des/#{citation_ctde.id}/citation/pdf#{tenant_suffix_params}",
            }
          }
        end                
      end

      def get_data_from_de(ctde,cmde)
        if cmde.data_type == DataElementType::LINK_TERM
          pramata_number = ctde.data_integer
          description = ctde.data_name 
          contract = Contract.where(pontus_number: pramata_number).first
          document_title = contract.display_name rescue ""
          {
            :pramata_number => pramata_number.to_s,
            :description => ctde.data_name,
            :document_title => document_title,
            :links => {
              :self => "#{base_url}/services/data/v1/contracts/#{pramata_number}#{tenant_suffix_params}"
            }
          }
        elsif cmde.data_type == DataElementType::FILE_TERM
          description = ctde.data_name.present? ? ctde.data_name : ctde.data_string
          {
            :description => description,
            :file_type => (ctde.data_string.split(".")[-1] rescue ""),
            :links => {
              :self => "#{base_url}/services/data/v1/contracts/#{pontus_number}/attachment/#{ctde.id}#{tenant_suffix_params}"
            }
          }            
        else
          ctde.send(cmde.get_data_column)
        end
      end

      # For category metadata information about ContractModelCategory 

      def categories
        if contract_model_id.blank?
          return []
        end

        categories_terms_with_positions = []
        tmp_term_ids = contract_terms.where(
          "( is_deleted is null or is_deleted = 0) AND amended_term_id is null")
        .select(
          "contract_model_term_type_id"
        ).collect{|ct| ct.contract_model_term_type_id}.uniq


        # Aplly user level and role level security
        current_contract_status = contract_po_status_type_id.nil? ? nil : contract_po_status_type
        cmtts = ContractModelTermType.where(id: tmp_term_ids)
        cmtts += ContractModelTermType.where("is_customer_editable_term=1")


        term_ids = []
        cmtts.each do |cmtt|
          # term_ids << cmtt.id
          # next
          # if !cmtt.nil? && cmtt.id != APP_CONFIG[:amendments_term_id] && cmtt.show_term_type(user_role) && cmtt.show_term_type_by_status(current_contract_status) && cmtt.show_ce_term(self,user_role,current_contract_status)
            term_ids << cmtt.id
          # end
        end

        category_terms_sql = "select cmtc.name as category_name,
        cmtt.name as term_name,
        cmtt.api_name as term_api_name,
        cmt.customer_term_tip as help_text,
        IF(cmtt.is_customer_editable_term=1,'Customer Editable',cmtt.display_type) as display_type,
        cmt.guideline_value as cmt_guideline_value,
        cmtt.guideline_value as cmtt_guideline_value,
        cmtt.id as cmtt_id

        from
        contract_model_term_categories cmtc inner join
        contract_model_terms cmt on cmt.contract_model_term_category_id=cmtc.id
        join contract_model_term_types cmtt on
        cmtt.id=cmt.contract_model_term_type_id where "
        if term_ids.present?
          condition_sql = "(cmt.contract_model_term_type_id in (#{term_ids.join(',')})) and "
        else
          condition_sql = ' 1 = 0 and '
        end
        condition_sql << " cmtc.name != 'Amendments' and cmt.contract_model_id=#{self.contract_model_id} order by
        cmtc.position,cmt.position"
        category_terms_sql << condition_sql
        categories_terms_with_positions = ActiveRecord::Base.connection.select_rows(category_terms_sql)
        get_metadata_categories_and_terms.each do |term|
          categories_terms_with_positions.unshift(term)
        end
        get_ordered_hash(categories_terms_with_positions)
      end
      def get_metadata_categories_and_terms
        meta_terms = []
        META_TERM_TYPES.each do |meta_term_type|
          key_term = meta_term_type.keys.first
          meta_terms << [
            "Overview",
            key_term,
            get_api_name(key_term),
            '',
            meta_term_type[key_term][:display_type]
          ]
        end
        meta_terms
      end
      def get_api_name(term_type_name)
        if term_type_name == "Customer"
          str = "Customer"
        else
          name = term_type_name.split(/[^A-Za-z0-9]/).map(&:capitalize).join("_").gsub(/[_]+/,'_')
          str = name.gsub(/^(.{95,}?).*$/m,'\1')
        end
        return "#{str}_Meta"
      end      
      def get_ordered_hash(categories_terms_with_positions)
        transformed_data = []
        categories_terms_with_positions.each {|data| transformed_data << {data[0] => data[1..-1]}}
        data = transformed_data.map{|i| [i.keys.first,i[i.keys.first]]}.group_by(&:first).map{|k,v| Hash[k, v.size.eql?(1) ? [v.last.last] : v.map(&:last) ]}
        transformed_data = []
        data.each do |category|
          category.each do |category_name,terms|
            terms_data = []
            terms.each do |term|
              terms_data << {"name" => term[0],"api_name" => term[1], "help_text" => term[2], "display_type" => term[3], "cmt_guideline_value" => term[4],"cmtt_guideline_value" => term[5], "id" => term[6]}
            end
            transformed_data << {"category" => category_name,"terms" => terms_data}
          end
        end
        return transformed_data
      end      
    end
  end
end