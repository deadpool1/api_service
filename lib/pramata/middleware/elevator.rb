require 'apartment/elevators/generic'
require 'jwt'
SECRET = "zIjcp4k_Cn1OVJpTby652Dt0Ix2lDcPVv_2dYRMrTnU5nyMx-lrAh0G6zsUz2UDMhk5gfO9hqzhhrbirx7Yw3Q01pZTOQb6nrle5i_nPqM2QcM-mNUT5PPIzez0I45OvXNRFAtLroyEphsPC39Ur-_TY1VAUzwwdJytTCZZbZW_SlgHlQu0Pt--HlNNOVYE12CDrX4kBVA8GAybuYsLwQ0oGjaF80i-dQRcRkXcireZusKhq2qofejfRtqZFkqvLQ0W6xOILuBdEdG5ZhJjy45ijiJVbM4RmhmcXGR8MGWl7RKbd-BJ1m2HWQyjGeX2RZFq57lzwvgAr5Hx6yrHKZg"
module Pramata
  module Middleware
    class Elevator < Apartment::Elevators::Generic
      def call(*args)
        begin
          super
        rescue Exception => e #Apartment::TenantNotFound
          raise e
          Rails.logger.error "ERROR: Apartment Tenant not found #{e.class} #{e.message} #{e.backtrace}"
          # return [404, {"Content-Type" => "application/json"}, ["Error" => "Tenant not found"]]
          # do something to notify that tenant is not found
          # [301, {'Location' => location, 'Content-Type' => 'text/html'}, ['Tenant Not Found']]
          [301, { 
            'Content-Type' => 'text/html'}, ["Tenant not found"]]
        end
      end

      def parse_credentials_from_jwt(request)
        jwt_token = request.get_header("HTTP_PRAMATAJWTTOKEN")
        decoded_token = JWT.decode jwt_token, SECRET, true, { :algorithm => 'HS256' }
        credentials = decoded_token[0]["data"]
        return credentials
      end

      def parse_tenant_name(request)
        credentials = parse_credentials_from_jwt(request)
        $tenant = Tenant.find_by_code(credentials["tenant_code"])      
        $current_role_id = credentials["role_id"]
        if $tenant.present?
          return $tenant.database 
        else
          raise Apartment::TenantNotFound, "Unable to find tenant"
        end
      end

    end
  end
end