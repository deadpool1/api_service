# README #

This README contains steps are necessary to get your application up and running.

### What is this repository for? ###

* This service exposes Digital Snapshots, Contracts API data in multi-tenant way 
* Version 0.1
* [Usecase](https://pramata.atlassian.net/browse/PA-409)

### How do I get set up? ###

* Install Ruby 2.4.1 and Rails 5.1
* Run `bundle install` 
* Install `yarn` using `npm install yarn -g` and run `yarn` on project folder
* Run `bundle exec rake db:setup` to setup databse
* Run `rspec` to validate the test cases are passing
* TODO: Deployment of the application in `AWS` needs to be finalized

### Contribution guidelines ###

* Code coverage should be 100% always (no matter what)
* Code review should follow [ruby guidelines](https://github.com/bbatsov/ruby-style-guide)
* Other guidelines

### Who do I talk to? ###

* Contact Project everest team for any queries
* Other community or team contact